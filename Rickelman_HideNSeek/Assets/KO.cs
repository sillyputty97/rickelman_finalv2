﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class KO : MonoBehaviour {

	Vector3 originalPos;

	// Use this for initialization
	void Start () {
		originalPos = transform.position;
		
	}
	
	// Update is called once per frame
	void Update () {

	

		}

	void OnCollisionEnter(Collision other){
		if (other.gameObject.CompareTag("Enemy")){
			transform.position = originalPos;
			Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);

		}

	}
}
